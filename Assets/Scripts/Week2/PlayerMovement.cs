using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float speedMultiplier;

    private Animator animator;

    private bool isUserInteractionEnabled;

    private Vector3 mousePosition;

    private void Start()
    {
        this.isUserInteractionEnabled = false;
        this.animator = this.GetComponent<Animator>();
    }

    private void Update()
    {
        if (this.animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f)
        {
            this.isUserInteractionEnabled = true;
            this.TriggerStartAnimation();
        }

        if (this.isUserInteractionEnabled)
        {
            this.MoveIfTriggered();
        }

        if (this.isUserInteractionEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.SetMousePosition();
            }

            if (Input.GetMouseButton(0))
            {
                this.RotateIfTriggered();
            }
        }
    }

    private void TriggerStartAnimation()
    {
        this.animator.SetBool("canWalk", true);
    }

    private void MoveIfTriggered()
    {
        var startPosition = this.gameObject.transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            var newVector3 = this.gameObject.transform.forward * speedMultiplier;
            this.gameObject.GetComponent<Transform>().localPosition += newVector3;
        }
    }

    private void SetMousePosition()
    {
        this.mousePosition = Input.mousePosition;
    }

    private void RotateIfTriggered()
    {
        var newMousePosition = Input.mousePosition;
        var deltaMousePosition = newMousePosition.x - this.mousePosition.x;
        var deltaMousePositionThreshold = 10f;

        if (deltaMousePosition < -deltaMousePositionThreshold || deltaMousePosition > deltaMousePositionThreshold)
        {
            this.gameObject.transform.eulerAngles += new Vector3(0f, deltaMousePosition * 0.01f, 0f);
        }
    }
}
