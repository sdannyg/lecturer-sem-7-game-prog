﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{

    [SerializeField]
    private float speed;

    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            this.startPosition = this.gameObject.transform.position;

            var newX = this.startPosition.x;
            var newY = this.startPosition.y;
            var newZ = this.startPosition.z + 1f * speed * Time.deltaTime;

            var newVector3 = new Vector3(
                newX,
                newY,
                newZ
            );

            this.gameObject.GetComponent<Transform>().position =
                newVector3;
        }
    }
}
